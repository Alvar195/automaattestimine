public class Calc {

    public static final int JUNIOR = 1;
    public static final int SENIOR = 2;
    public static final int SPECIALIST = 3;

    public static final int JUNIOR_PAY = 10;
    public static final int SENIOR_PAY = 15;
    public static final int SPECIALIST_PAY = 22;

    private int sum;

    protected int pay(final int type, final int hours) {

        sum = 0;

        calculateDailyPay(type, hours);
        if (hours > 20) {
            heroBonus(type);
        }

        return sum;
    }

    private void calculateDailyPay(final int type, int hours) {

        if (type == JUNIOR) {
            payJunior(hours);
        }
        if (type == SENIOR) {
            paySenior(hours);
        }
        if (type == SPECIALIST) {
            paySpecialist(hours);
        }

    }
    

    private void payJunior(int hours) {
        if (hours > 8) { //need eraldi meetoditesse
            sum = JUNIOR_PAY * (hours - 8) * 2;
            sum += JUNIOR_PAY * 8;
        } else {
            sum += JUNIOR_PAY * hours;
        }
    }

    private void paySenior(int hours) {
        if (hours > 8) {
            sum = SENIOR_PAY * (hours - 8) * 2;
            sum += SENIOR_PAY * 8;
        } else {
            sum += SENIOR_PAY * hours;
        }
    }

    private void paySpecialist(int hours) {
        if (hours > 9) {
            sum = SPECIALIST_PAY * (hours - 9) * 3;
            sum += SPECIALIST_PAY * 9;
        } else {
            sum += SPECIALIST_PAY * hours;
        }
    }

    private void heroBonus(final int type) {

        if (type == JUNIOR) {
            sum += 10;
        }
        if (type == SENIOR) {
            sum += 20;
        }
        if (type == SPECIALIST) {
            sum += 30;
        }
    }
}