import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by alvar on 02.11.2015.
 */
public class CalcTest {

    public Calc calc;

    @Before
    public void setUp() throws Exception {
        calc = new Calc();
    }

    @Test
    public void testPay() throws Exception {
        assertEquals(66, calc.pay(3, 3));
        assertEquals(396, calc.pay(3, 12));
        assertEquals(1086, calc.pay(3, 22));
        assertEquals(15, calc.pay(2, 1));
        assertEquals(180, calc.pay(2, 10));
        assertEquals(590, calc.pay(2, 23));
        assertEquals(10, calc.pay(1, 1));
        assertEquals(100, calc.pay(1, 9));
        assertEquals(390, calc.pay(1, 23));
    }
}